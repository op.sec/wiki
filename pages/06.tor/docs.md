---
title: Tor
visible: true
---

### What is Tor?
[Tor](https://www.torproject.org) is free software and an open network that helps you defend against traffic analysis, a form of network surveillance that threatens personal freedom and privacy, confidential business activities and relationships, and state security.

### Why Anonymity Matters
[Tor](https://www.torproject.org) protects you by bouncing your communications around a distributed network of relays run by volunteers all around the world: it prevents somebody watching your Internet connection from learning what sites you visit, and it prevents the sites you visit from learning your physical location.

[Download Tor (en)](https://www.torproject.org/download/download.html.en)

[Download Tor (fr)](https://www.torproject.org/download/download.html.fr)

[Animation Video Explaining Tor (en)](https://media.torproject.org/video/2015-03-animation/HQ/Tor_Animation_en.mp4)

[Animation Video Explaining Tor (fr)](https://media.torproject.org/video/2015-03-animation/HQ/Tor_Animation_en.mp4)