---
title: Intro
taxonomy:
    category:
        - chapter
child_type: chapter
---

## Digital Operational Security Wiki

This wiki to intended to provide up to date digital operational security advice for activists, journalists or anyone that may benefit from it.

Feel free to contribute content with the edit button above or contact <opsec@msgsafe.io>.

**Status**: _Alpha (Under Development)_
