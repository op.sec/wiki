---
title: Laptop
---

### Laptop Apps

[Tor Browser (Anonymous Browser)](https://torproject.org)

[Tails OS (Anonymous OS)](https://tails.boum.org)

[UBlock (Ad Blocker)](https://www.ublock.org)

[DuckDuckGo (Search)](https://duckduckgo.com)