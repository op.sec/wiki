---
title: Mobile
taxonomy:
    category:
        - docs
visible: true
---

### Android Apps

[Signal Private Messenger](https://www.signal.org/)

[Silence (Encrypted SMS)](https://f-droid.org/packages/org.smssecure.smssecure/)

[Bitmask (Free VPN Riseup)](https://f-droid.org/en/packages/se.leap.bitmaskclient/)

[Tor (Anonymous Internet)](https://f-droid.org/repository/browse/?fdfilter=riot&fdid=org.torproject.android)

[Tor Browser](https://f-droid.org/repository/browse/?fdfilter=orfox&fdid=info.guardianproject.browser)

[Riot (matrix)](https://f-droid.org/packages/im.vector.alpha/)

[Briar (Peer2Peer Encrypted Comms)](https://briarproject.org/)

[Nextcloud (File Cloud Storage / Photos)](https://f-droid.org/packages/com.nextcloud.client/)

[DavDroid (Cal + Contacts Sync)](https://f-droid.org/packages/at.bitfire.davdroid/)

[F-Droid (Open Source Apps)](https://f-droid.org)

[KeePass (Password Manager)](https://f-droid.org/repository/browse/?fdid=com.android.keepass)

[Yalp (Google App Store)](https://f-droid.org/packages/com.github.yeriomin.yalpstore/)