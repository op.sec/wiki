---
title: Decentralization
---

Free yourself from centralized service's like google, apple, facebook, twitter, dropbox etc. 

Here are some free open source [decentralized](https://en.m.wikipedia.org/wiki/Decentralization) platforms.

[Disroot](https://disroot.org)

[Disroot HowTos](https://howto.disroot.org)

[Matrix](https://matrix.org)

[Mastodon](https://mastodon.social/about)